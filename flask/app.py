import sqlite3
from flask import Flask, render_template, request

app = Flask(__name__)
DB_name = 'db_ml_flask.db'

# zapros na vyvod biblioteki
def cursor_fetchall(arg1, arg2=()):
    with sqlite3.connect(DB_name) as conn:
        cursor = conn.cursor()
        cursor.execute(arg1, arg2)
        data = cursor.fetchall()
    return data

# zapros na izmeneniya v biblioteke
def conn_commit(arg1, arg2):
    with sqlite3.connect(DB_name) as conn:
        cursor = conn.cursor()
        cursor.execute(arg1, arg2)
        conn.commit()

@app.route('/', methods=['GET', 'POST'])
def music_lib():
    var_list = ['name', 'band', 'album', 'genre', 'year']
    get_id = request.values.get('delete')
    insert_new = request.values.get('insert_new')
    update_song = request.values.get('update')
    search_song = request.values.get('search')
    # redaktirovanie pesni
    if update_song:
        update_song = [request.values.get(x) for x in var_list]
        update_song.append(request.values.get('update'))
        if update_song[0] or update_song[1] or update_song[3]:
            conn_commit('update song set name=?, band=?, album=?, genre=?, year=? where id=?;', update_song)
    # Dobavlenie novoi pesni
    if insert_new:
        insert_new = [request.values.get(x) for x in var_list]
        if insert_new[0] or insert_new[1] or insert_new[3]:
            conn_commit('insert into song (name, band, album, genre, year) values (?,?,?,?,?);', insert_new)
    # Ydalenie pesti
    if get_id:
        conn_commit('DELETE FROM Song WHERE id = ?;', (get_id,))
    # Poisk
    if search_song:
        # Poisk po imeni pisni
        if search_song == 'name':
            search_song = request.values.get(search_song)
            data = cursor_fetchall('select * from song where name like ?;', ('%' + search_song + '%',))
            return render_template('index.html', context=data)
        # Poisk po imeni ispolnitelya \ gruppy
        elif search_song == 'band':
            search_song = request.values.get(search_song)
            data = cursor_fetchall('select * from song where band like ?;', ('%' + search_song + '%',))
            return render_template('index.html', context=data)
    # Vyvod spiska vseh pesen
    data = cursor_fetchall('select * from song;',)
    return render_template('index.html', context=data)


if __name__ == '__main__':
    app.run(host="127.0.0.1", port=5000, debug=True)
