def parse(query: str) -> dict:
    rdict = {}
    if len(query.split('?')) <= 1: return rdict
    rdict = {item.split('=')[0]: item.split('=')[1] for item in ((query.split('?')[1]).split('&')) if (len(item) > 2)}
    return rdict


if __name__ == '__main__':
    assert parse('https://example.com/path/to/page?name=ferret&color=purple') == {'name': 'ferret', 'color': 'purple'}
    assert parse('https://example.com/path/to/page?name=ferret&color=purple&') == {'name': 'ferret', 'color': 'purple'}
    assert parse('http://example.com/') == {}
    assert parse('http://example.com/?') == {}
    assert parse('http://example.com/?name=Dima') == {'name': 'Dima'}


def parse_cookie(query: str) -> dict:
    rdict = {}
    if len(query.split(';')) == -1: return rdict
    rdict = {item.split('=', 1)[0]: item.split('=', 1)[1] for item in (query.split(';')) if (len(item) > 0)}
    return rdict


if __name__ == '__main__':
    assert parse_cookie('name=Dima;') == {'name': 'Dima'}
    assert parse_cookie('') == {}
    assert parse_cookie('name=Dima;age=28;') == {'name': 'Dima', 'age': '28'}
    assert parse_cookie('name=Dima=User;age=28;') == {'name': 'Dima=User', 'age': '28'}
