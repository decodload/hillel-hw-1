from django import forms
from django.contrib.auth import authenticate, login, get_user_model
from django.contrib.auth.models import User
from django.utils import timezone
from mbsapp.models import Student, Teacher, Alert


# Add Student by Form without ModelForm and FormView


class AddStudentForm(forms.ModelForm):
    class Meta:
        model = Student
        fields = '__all__'  # or exclude = ('name_field, name_field') eto dlya isclucheniya polei -> vse krome ykazannyh

    # Validaciya na NEsovpadenie familii 'surname' v DB
    def clean_name(self):
        name = self.cleaned_data['name']
        matching_names = Student.objects.values('id', 'name').filter(name__icontains=name)
        surname = name.split(' ')
        # Proverka 'first and last name' i sovpadenii s DB
        if len(matching_names) == 0 and len(surname) <= 1:
            raise forms.ValidationError('Enter the first and last name and then try again.')
        elif len(matching_names) > 0:
            for item in matching_names:
                surname_students = item['name'].split(' ')
                # Esli obnovlyaem dannye studenta - poisk sovpadenii:
                if len(surname) > 1 and len(surname_students) > 1 and 'id' in self.initial:
                    if surname[1].lower() == surname_students[1].lower() and self.initial['id'] != item['id']:
                        raise forms.ValidationError('Unfortunately a student with that Surname already exists.')
                # Esli dobavlen novyi student - otsutstvuet ID:
                elif len(surname) > 1 and len(surname_students) > 1 and 'id' not in self.initial:
                    raise forms.ValidationError('Unfortunately a student with that Surname already exists.')
        return name


# Add Teacher by Form with ModelForm
class AddTeacherForm(forms.ModelForm):
    class Meta:
        model = Teacher
        fields = '__all__'  # or exclude = ('name_field, name_field') eto dlya isclucheniya polei -> vse krome ykazannyh


class RegistrationForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)
    check_password = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = get_user_model()
        fields = (
            'first_name',
            'last_name',
            'username',
            'email',
        )

    def clean_check_password(self):
        if self.cleaned_data['password'] != self.cleaned_data['check_password']:
            raise forms.ValidationError('Pass don\'t match')
        return self.cleaned_data['check_password']

    def save(self, commit=True):
        user = super(RegistrationForm, self).save(commit=False)
        user.set_password(self.cleaned_data['password'])
        user.save()
        return user


class AlertForm(forms.ModelForm):

    class Meta:
        model = Alert
        exclude = ('publish_date',)

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user')
        super(AlertForm, self).__init__(*args, **kwargs)

    def save(self, *args, **kwargs):
        alert = super(AlertForm, self).save(commit=False)

        if self.cleaned_data['is_published']:
            alert.publish_date = timezone.now()

        alert.author = self.user
        alert.save()
        self.save_m2m()
        return alert


# Obrazets dlya praktiki
# class LoginForm(forms.Form):
#     username = forms.CharField()
#     password = forms.CharField(widget=forms.widgets.PasswordInput())
#
#     def clean(self):
#         # try:
#         #     User.objects.values('id').get(
#         #         username=self.cleaned_data['username'],
#         #         password=self.cleaned_data['password']
#         #     )
#         # except User.DoesNotExist:
#         #     raise forms.ValidationError('Incorrect username or password')
#
#         user = User.objects.filter(
#             username=self.cleaned_data['username']
#         ).first()
#
#         if not user or not user.check_password(self.cleaned_data['password']):
#             raise forms.ValidationError('Incorrect username or password')
#
#     def auth(self, request):
#         user = authenticate(request, **self.cleaned_data)
#         login(request, user)
#         return user
