from django.core.mail import send_mail
from membership.celery import app
from datetime import datetime, timedelta


@app.task
def send_alerts_to_subscribers(alert_id):
    from mbsapp.models import Group, Alert

    alert = Alert.objects.get(id=alert_id)
    emails = []
    target_audience = alert.target_audience.values_list('subject_name', flat=True)

    for element in target_audience:
        subscriber_email = Group.objects.get(subject_name=element).subject_subscribers.all().values_list('email', flat=True)
        result = [x for x in subscriber_email if x not in emails]
        emails.extend(result)

    for subscriber_email in emails:
        send_mail(
            f'New Alert from {alert.author.username}',
            f'Hello from {alert.author.username}',
            'admin@admin.com',
            recipient_list=[subscriber_email]
        )

@app.task
def new_subject_notiofications():
    from mbsapp.models import Group, CustomUser

    new_subjects = ", ".join(map(str, Group.objects.filter(publish_date__gte=datetime.now() - timedelta(days=7)).order_by('publish_date').values_list('subject_name', flat=True)))
    subscriber_email = CustomUser.objects.all().values('email')

    for emails in subscriber_email:
        send_mail(
            f'New Alert from Membership app',
            f'We have some new items in the last week. Check them out: {new_subjects}',
            'admin@admin.com',
            recipient_list=[emails['email']]
        )
