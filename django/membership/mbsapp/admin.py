from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import gettext_lazy as _
from mbsapp.models import Student, Group, Teacher, CustomUser, Alert
# Register your models here.

class UserProfileAdmin(UserAdmin):
    fieldsets = (
        (None, {"fields": ("username", "password")}),
        (_("Personal info"), {"fields": ("first_name", "last_name", "email", "phone")}),
        (
            _("Permissions"),
            {
                "fields": (
                    "is_active",
                    "is_staff",
                    "is_superuser",
                    "groups",
                    "user_permissions",
                ),
            },
        ),
        (_("Important dates"), {"fields": ("last_login", "date_joined")}),
        (_("Other"), {"fields": ("subscribers",)}),
    )


admin.site.register(Student)
admin.site.register(Group)
admin.site.register(Teacher)
admin.site.register(Alert)
admin.site.register(CustomUser, UserProfileAdmin)
