from django.apps import AppConfig


class MbsappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'mbsapp'
