"""membership URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from mbsapp import views
from django.contrib.auth.views import LoginView, LogoutView
from django.urls import reverse_lazy

urlpatterns = [
    path('', views.IndexView.as_view(), name='home'),
    path('generate-student/', views.StudentView.as_view(), name='generate-student'),
    path('generate-students/', views.GenerateStudentView.as_view(), name='generate-students'),
    path('group/', views.GroupView.as_view(), name='group'),
    path('teacher/', views.TeacherView.as_view(), name='teacher'),
    path('add-student/', views.AddStudentView.as_view(), name='add-student'),
    path('add-teacher/', views.AddTeacherView.as_view(), name='add-teacher'),
    path('student-update/<int:id>/', views.StudentUpdateView.as_view(), name='student-update'),
    path('teacher-update/<int:id>/', views.TeacherUpdateView.as_view(), name='teacher-update'),
    path('login/', LoginView.as_view(template_name='mbsapp/login.html', next_page='/'), name='login'),
    path('logout/', LogoutView.as_view(next_page='/'), name='logout'),
    path('registration/', views.RegistrationView.as_view(), name='registration'),
    path('profile/<int:id>/', views.ProfileView.as_view(), name='profile'),
    path('group/<int:id>/', views.SubjectSubscribeView.as_view(), name='group-subscribe'),
    path('add-alert/', views.AlertCreateView.as_view(), name='add-alert'),

]
