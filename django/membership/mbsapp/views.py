import random

from django.contrib.auth import get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.shortcuts import render, get_object_or_404, redirect
from django.urls import reverse_lazy
from django.views.generic import TemplateView, FormView, UpdateView, DetailView, CreateView
from django.views import View
from faker import Faker
from mbsapp.models import Student, Group, Teacher, CustomUser, Alert
from mbsapp.forms import AddStudentForm, AddTeacherForm, RegistrationForm, AlertForm

# Create your views here.

fake = Faker()


class IndexView(TemplateView):
    model = Student
    template_name = 'mbsapp/index.html'

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        students = Student.objects.all()
        context.update({
            'students': students,
            'title': 'Home Page',
        })
        return context


class StudentView(TemplateView):
    model = Student
    template_name = 'mbsapp/generate-student.html'

    # Obrazets dispatch dlya redirecta na avtorizatsiyu bez nasledovaniya LoginRequiredMixin. Kod identichen LoginRequiredMixin
    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return redirect(f'/login/?next={request.path}')
        return super(StudentView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(StudentView, self).get_context_data(**kwargs)
        Student.objects.create(name=fake.name(), marks=random.randint(50, 100))
        students = Student.objects.all()
        context.update({
            'students': students,
            'title': 'Generate one student',
        })
        return context


class GenerateStudentView(LoginRequiredMixin, TemplateView):
    model = Student
    template_name = 'mbsapp/generate-students.html'
    login_url = '/login/'  # Peredaem adres redirecta v LoginRequiredMixin

    def get_context_data(self, **kwargs):
        context = super(GenerateStudentView, self).get_context_data(**kwargs)
        if 'amount' in self.request.GET and self.request.GET['amount'] != '' and int(
                self.request.GET['amount']) > 0 and int(self.request.GET['amount']) <= 100:
            amount = int(self.request.GET['amount'])
            if amount:
                # gstudents - "generate students"
                gstudents = [{'name': fake.name(), 'marks': random.randint(50, 100)} for item in range(int(amount)) if
                             item < int(amount)]
                for item in gstudents:
                    Student.objects.create(name=item['name'], marks=item['marks'])
        students = Student.objects.all()
        context.update({
            'students': students,
            'title': 'Generate multiple students',
        })
        return context


class GroupView(TemplateView):
    model = Group
    template_name = 'mbsapp/group.html'

    def get_context_data(self, **kwargs):
        context = super(GroupView, self).get_context_data(**kwargs)
        groups = Group.objects.all()
        count_subject = Group.objects.filter(subject_subscribers=self.request.user.id).count()
        context.update({
            'groups': groups,
            'title': 'Groups',
            'count_subject': count_subject,
        })
        return context


class TeacherView(TemplateView):
    model = Teacher
    template_name = 'mbsapp/teacher.html'

    def get_context_data(self, **kwargs):
        context = super(TeacherView, self).get_context_data(**kwargs)
        teachers = Teacher.objects.all()
        context.update({
            'teachers': teachers,
            'title': 'Teachers',
        })
        return context


class AddStudentView(FormView):
    template_name = 'mbsapp/add-student.html'
    form_class = AddStudentForm
    success_url = reverse_lazy('add-student')

    def get_context_data(self, **kwargs):
        context = super(AddStudentView, self).get_context_data(**kwargs)
        students = Student.objects.all()
        context.update({
            'students': students,
            'title': 'Add one Student',
        })
        return context

    def form_valid(self, form):
        form.save()
        return super(AddStudentView, self).form_valid(form)


# Add Teacher by Form with FormView
class AddTeacherView(FormView):
    template_name = 'mbsapp/add-teacher.html'
    form_class = AddTeacherForm
    success_url = reverse_lazy('add-teacher')

    def get_context_data(self, **kwargs):
        context = super(AddTeacherView, self).get_context_data(**kwargs)
        teachers = Teacher.objects.all()
        context.update({
            'teachers': teachers,
            'title': 'Add one Teacher',
        })
        return context

    def form_valid(self, form):
        form.save()
        return super(AddTeacherView, self).form_valid(form)


class StudentUpdateView(FormView):
    template_name = 'mbsapp/add-student.html'
    form_class = AddStudentForm
    success_url = reverse_lazy('add-student')

    def get_context_data(self, **kwargs):
        context = super(StudentUpdateView, self).get_context_data(**kwargs)
        students = Student.objects.all()
        context.update({
            'students': students,
            'title': 'Update Student',
        })
        return context

    def form_valid(self, form):
        form.save()
        return super(StudentUpdateView, self).form_valid(form)

    def get_form_kwargs(self):
        form_kwargs = super(StudentUpdateView, self).get_form_kwargs()
        form_kwargs['instance'] = get_object_or_404(Student, id=self.kwargs['id'])
        return form_kwargs


class TeacherUpdateView(UpdateView):
    template_name = 'mbsapp/add-teacher.html'
    model = Teacher
    form_class = AddTeacherForm
    success_url = reverse_lazy('add-teacher')
    pk_url_kwarg = 'id'

    def get_context_data(self, **kwargs):
        context = super(TeacherUpdateView, self).get_context_data(**kwargs)
        teachers = Teacher.objects.all()
        context.update({
            'teachers': teachers,
            'title': 'Update Teacher',
        })
        return context


class RegistrationView(FormView):
    template_name = 'mbsapp/registration.html'
    form_class = RegistrationForm
    success_url = reverse_lazy('login')

    def form_valid(self, form):
        form.save()
        return super(RegistrationView, self).form_valid(form)


class ProfileView(DetailView):
    model = get_user_model()
    template_name = 'mbsapp/profile.html'
    pk_url_kwarg = 'id'

    def post(self, request, id):
        current_user = get_object_or_404(get_user_model(), id=id)
        if request.user not in current_user.subscribers.all():
            current_user.subscribers.add(request.user)
        else:
            current_user.subscribers.remove(request.user)
        current_user.save()
        return redirect(request.path)


# Podpiska na predmety kyrsov <= 5
class SubjectSubscribeView(DetailView):
    model = Group
    template_name = 'mbsapp/group.html'
    pk_url_kwarg = 'id'

    def get_context_data(self, **kwargs):
        context = super(SubjectSubscribeView, self).get_context_data(**kwargs)
        groups = Group.objects.all()
        count_subject = Group.objects.filter(subject_subscribers=self.request.user.id).count()
        context.update({
            'groups': groups,
            'title': 'Groups',
            'count_subject': count_subject,
        })
        return context

    def post(self, request, id):
        current_subject = get_object_or_404(Group, id=id)
        count_subject = Group.objects.filter(subject_subscribers=request.user.id).count()
        if count_subject < 5 and request.user not in current_subject.subject_subscribers.all():
            current_subject.subject_subscribers.add(request.user)
        else:
            current_subject.subject_subscribers.remove(request.user)
        current_subject.save()
        return redirect(request.path)


class AlertCreateView(CreateView):
    template_name = 'mbsapp/add-alert.html'
    model = Alert
    form_class = AlertForm
    success_url = '/'

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return redirect(f'/login/?next={request.path}')
        return super(AlertCreateView, self).dispatch(request, *args, **kwargs)

    def get_form_kwargs(self):
        form_kwargs = super(AlertCreateView, self).get_form_kwargs()
        form_kwargs['user'] = self.request.user
        return form_kwargs


# Obrazets dlya praktiki
# class LoginView(FormView):
#     form_class = LoginForm
#     template_name = 'mbsapp/login.html'
#     success_url = '/'
#
#     def form_valid(self, form):
#         form.auth(self.request)
#         return super(LoginView, self).form_valid(form)
