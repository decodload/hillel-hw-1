from django.contrib.auth import get_user_model
from django.contrib.auth.models import User, AbstractUser
from django.core.mail import send_mail
from django.db import models
from mbsapp.tasks import *

# Create your models here.
from django.db.models.signals import post_save
from django.dispatch import receiver


class TimeIt(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Student(models.Model):
    name = models.CharField(max_length=255)
    marks = models.IntegerField()

    def __str__(self):
        return self.name


class Group(models.Model):
    subject_name = models.CharField(max_length=255)
    subject_type = models.CharField(max_length=255)
    publish_date = models.DateTimeField(blank=True, null=True)
    subject_subscribers = models.ManyToManyField('mbsapp.CustomUser', related_name='subjects', blank=True)

    def __str__(self):
        return self.subject_name


class Teacher(models.Model):
    name = models.CharField(max_length=255)
    subject_name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class CustomUser(AbstractUser):
    phone = models.CharField(max_length=11, blank=True, null=True)
    subscribers = models.ManyToManyField('mbsapp.CustomUser', blank=True)


class Alert(TimeIt):
    title = models.CharField(max_length=255)
    body = models.TextField()
    publish_date = models.DateTimeField(blank=True, null=True)
    is_published = models.BooleanField(default=False)
    author = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    target_audience = models.ManyToManyField('mbsapp.Group')

    def __str__(self):
        return self.title


# @receiver(<signal>, sender=<Alert>)
@receiver(post_save, sender=Alert)
def new_course_info_handler(instance, **kwargs):

    if instance.is_published:
        send_alerts_to_subscribers.delay(instance.id)
