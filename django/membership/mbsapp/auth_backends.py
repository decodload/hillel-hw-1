from django.contrib.auth.backends import ModelBackend
from django.contrib.auth.models import User
from mbsapp.models import CustomUser


class EmailAuthBackend(ModelBackend):

    def authenticate(self, request, username=None, password=None, **kwargs):
        user = CustomUser.objects.filter(email=username).first()

        if user and user.check_password(password) and self.user_can_authenticate(user):
            return user
