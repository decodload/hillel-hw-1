from django import template
from mbsapp.models import Teacher

register = template.Library()

# Vozvrashyaet kolichestvo slov
@register.filter()
def amount_words(text):
    wordlist = text.count(' ') + 1
    text = text + ' | Amount words: ' + wordlist
    return text

# Vozvrashaet spisok chetnyh chisel iz list_number
@register.filter()
def even_number_list(list_number):
    result = list(filter(lambda x: isinstance(x, int), list_number))
    result = [x for x in result if x % 2 == 0]
    return result

# Vozvrashaet spisok chetnyh chisel iz texta
@register.filter()
def even_number_text(text):
    numberlist = text.replace(' ', '').split(',')
    numberlist = [int(x) for x in numberlist if x.isnumeric()]
    result = [x for x in numberlist if x % 2 == 0]
    return result

# Vyvodit kolichestvo slov, proverka na napisenie imeni i familii
@register.filter()
def name_is_correct(text, color='red'):
    wordlist = text.count(' ') + 1
    if wordlist > 2:
        text = f"<span style='color:{color}'>{text}</span> | Amount words: <span style='color:{color}'>{wordlist}</span>"
    else:
        text = text + f" | Amount words: <span style='color:green'>{wordlist}</span>"
    return text

# Vyvodit gradient uspevaemosti po EN sisteme ocenivaniya
@register.filter()
def assessment_table(text):
    mark = text
    text = str(text)
    if mark >= 90 and mark <= 100:
        text = text + ' | A (Excellent)'
    elif mark >= 82 and mark <= 89:
        text = text + ' | B (Very good)'
    elif mark >= 75 and mark <= 81:
        text = text + ' | C (Good)'
    elif mark >= 64 and mark <= 74:
        text = text + ' | D (Average)'
    elif mark >= 60 and mark <= 63:
        text = text + ' | E (Acceptable)'
    elif mark >= 35 and mark <= 59:
        text = text + ' | FX (Below average)'
    else:
        text = text + ' | F (Fail)'
    return text

# Peredaet 5 randomnyh uchitelei v tsikl 'for'
@register.simple_tag()
def random_teacher():
    teacher_list = Teacher.objects.all().order_by('?')[:5]
    if not teacher_list:
        return {}
    return teacher_list
